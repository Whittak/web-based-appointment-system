# README #

### How do I get set up? ###

For Windows:

 Browse to https://bitbucket.org/Whittak/web-based-appointment-system/src.
 Click on the WebBasedAppt_SQLite-0.0.1-SNAPSHOT-spring-boot.jar link.
 Click View raw link - this should download the .jar file to your system.
 Browse to the file location and double click it.
 A web_based_appt.db file should be create in the same location.
 In your browser enter the url localhost:8081.
 For each login option (Administrator, Doctor and Patient) the details are the same: User Name: user, Password: password.

For Linux:

 Download the .jar file in the same manner as above.
 Browse to file location, right click the .jar file and ensure it is allow the file to be executed.
 Open a CLI and enter: java -jar WebBasedAppt_SQLite-0.0.1-SNAPSHOT-spring-boot.jar
 Browse to the same url address to access the application.
